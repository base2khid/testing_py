#!/usr/bin/env python3
import os
import sys
import platform

from clint.textui import colored
from pathlib import Path

host        = platform.node()
pwd         = os.getcwd();
md5_dir     = pwd + '/md5/';

if host == 'raspi':
    data_dir    = '/mnt/usb-crypt/';
else:
    if host == 'truenas.local':
        data_dir    = '/mnt/vdev0/storage/';
    else:
        print(colored.red('\n --- Host "'+host+'" not known. please set your data_dir ... ABORT ...\n'));
        sys.exit();

wrk_dirs    = ('Backup_Disk', 'Backup_Disk_root');
    
print('--------------------------------------------------------------------');
print(' Host     : '+host);
print(' md5_dir  : '+md5_dir + colored.yellow(' - md5 is in wrk_dir -'));
print(' data_dir : '+data_dir);
print('--------------------------------------------------------------------');

for x in range(len(wrk_dirs)):
    wrk_dir = data_dir+wrk_dirs[x];
    
    print(' '+"{0:0=3d}".format(x)+' - Reading datadir  : '+colored.green(wrk_dirs[x]));
    print(' Working Dir            : '+wrk_dir);
    print(' Sort order             : newest on top');
    print('--------------------------------------------------------------------');
    
    # ls in working dirs
    ls_list = os.listdir(data_dir+wrk_dirs[x]+'/');
    for x in range(len(ls_list)):
        ls_list[x] = wrk_dir+'/'+ls_list[x];
        
    ls_list.sort(key=os.path.getmtime, reverse=True)
    for x in range(len(ls_list)):
        wrk_data_path   = ls_list[x];
        mtime           = os.popen('stat -c "%y" '+wrk_data_path).read()[:19];
        ftype           = os.popen('stat -c "%F" '+wrk_data_path).read()[:-1];
        int_year        = int(mtime[:4]);
        
        print(' {0:47}'.format(ls_list[x]), end='');
        if int_year == 2022:
            print(colored.green(' {0:19}').format(mtime), end='');
        else:
            print(colored.red(' {0:19}').format(mtime), end='');
        print(' {0:19}'.format(ftype));
        
    print('--------------------------------------------------------------------');
    print();
    
print('\nGoing to md5dir : '+md5_dir);
print('--------------------------------------------------------------------');

ls_list = os.listdir(md5_dir);

# make full path
for x in range(len(ls_list)):
    print(md5_dir+ls_list[x]);
    ls_list[x] = md5_dir+ls_list[x];
    
# sort list    
ls_list.sort(key=os.path.getmtime, reverse=True)

print('\n- List Sorted. Newest up.');
print('--------------------------------------------------------------------');


for x in range(len(ls_list)):
    fp = open(ls_list[x], 'r', True);
    for x in fp.readlines():
        xs      = x.split('  ');
        md5     = xs[0];
        name    = xs[1];
        pv_cmd  = 'pv -i 0.1 -w '+str(len(name)+14)+' '+name[:-1]+' | md5sum';
        
        print('\nreading file : '+name[:-1]);
        pv_out  = os.popen(pv_cmd).read();
        print(md5+' - md5 read from file');
        if pv_out[:-4] == md5:
            print(colored.magenta(pv_out[:-4]), end='');
            print(colored.green(' - md5 OK'));
        else:
            print(colored.magenta('"'+pv_out[:-4]+'"'), end='');
            print(colored.red(' - md5 NOT OK'));
     
    fp.close();

print();    
