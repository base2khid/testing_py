#!/usr/bin/env python3
import os
import sys
import hashlib
import math
import time

# from pyblake2   import blake2b
# from pyblake2   import blake2s
from pathlib    import Path

from clint.textui import colored

# -----------------------------------------------------------------------------
# - get digest of file
# -----------------------------------------------------------------------------
def get_digest(file_path):
    h = hashlib.sha256()
    # h = hashlib.blake2s256()
    # h = blake2s()

    with open(file_path, 'rb') as file:
        while True:
            # Reading is buffered, so we can read smaller chunks.
            chunk = file.read(h.block_size)
            if not chunk:
                break
            h.update(chunk)
            
    file.close();
    return h.hexdigest()

# -----------------------------------------------------------------------------
# - convert filesize from bytes to ...
# -----------------------------------------------------------------------------
def convert_size(size_bytes):
    if size_bytes == 0:
        return "0B"
    size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
    i = int(math.floor(math.log(size_bytes, 1024)))
    p = math.pow(1024, i)
    s = round(size_bytes / p, 2)
    return "%s %s" % (s, size_name[i])
   

# -----------------------------------------------------------------------------
# - this should be main
# -----------------------------------------------------------------------------
def main():

    print (sys.argv);

    # f = open("2021_files.txt");
    if len(sys.argv) > 1:
        f = open(sys.argv[1]);
    else: 
        print("Exit: File open Error.");
        sys.exit();
    
    print(f);
    print("------------------------------------------------");
    print("open file: ", sys.argv[1]);
    # print();
    # print();

    all_size = 0;
    for i in f.readlines():
        print();
        
        # Filename
        filename = i[:-1];                                          # read filename from infile. strip \n
        cmd1 = 'basename \"' + filename + "\"";                     # format the command
        print("Filename  : ",end = '');                             # msg: Filename
        file = "\"" + os.popen(cmd1).read()[:-1] + "\"";            # get basename of file
        print(file);                                                # print the base_filename
        print("------------------------------------------------");

        # check if testfile exists
        # if os.path.isfile(hashfile[1:-1]):
        if os.path.isfile(filename):
            print(colored.green(" File OK   : ") + filename);
        else:
            print(colored.red(" file DONT exists : ") + filename + "\n");
            sys.exit();
        
        # mTime
        cmd1 = 'stat -c "%y" \"' + filename + "\"";
        #print(cmd1);
        print(" mTime     : ",end = '');
        mtime = "\"" + os.popen(cmd1).read()[:-1] + "\"";
        print(colored.yellow(mtime));
        
        # filesize
        cmd1 = 'stat -c "%s" \"' + filename + "\"";
        print(" Size      : ",end = '');
        size = int(os.popen(cmd1).read()[:-1]);
        all_size = all_size + size;
        # print(int(size)/1024/1024);
        # print(convert_size(int(size)));
        mysize = convert_size(size);
        print(colored.green(mysize));
        
        # hashfile
        pwd = os.popen('pwd').read()[:-1]+'/md5/';
        # print(pwd);
        if os.path.isdir(pwd):    
            # os.system('touch '+pwd+'testfile');
            # print(colored.green(' Hashdir exists ...'));
            pass;
        else:
            print(colored.red('NO Hashdir'));
            print(colored.yellow('i try to make it...'));
            os.system('mkdir -p '+pwd);
            
            if os.path.isdir(pwd):    
                os.system('touch '+pwd+'testfile');
                print(colored.yellow('Hashdir created ...'));
                os.system('rm '+pwd+'testfile');
            else:
                print(colored.red('NO Hashdir... second time ... ARORTING'));
            
        hashfile = '"'+ pwd + file[1:-1] +'.md5"';
        # hashfile = "\"/mnt/RAMDisk/" + file[1:-1] + ".md5\"";
        # hashfile = "\"/mnt/RAMDisk/" + file[1:-4] + "blake2s\"";
        # hashfile = "/mnt/RAMDisk/" + file[1:-4] + "blake2b";
        print(" Hashfile  : ",end = '');
        print(hashfile);
        
        # Directory
        print(" ------------------------------------------------");
        cmd2 = 'dirname \"' + filename + "\"";
        #print(cmd2);
        print(" Directory : ", end = '');
        dir = "\"" + os.popen(cmd2).read()[:-1] + "\"";
        print(dir);

        # Dir mTime
        cmd1 = 'stat -c "%y" ' + dir + "";
        #print(cmd1);
        print(" mTime     : ",end = '');
        mtime = "\"" + os.popen(cmd1).read()[:-1] + "\"";
        print(mtime);
        print(" ------------------------------------------------");
        
        
        # hf = Path("./" + hashfile);
        # if(hf.is_file()):
        # if(os.path.isfile("./" + hashfile)):
        if os.path.isfile(hashfile[1:-1]):
            print(colored.green(" File OK   : "), end = '');
            hf = open(hashfile[1:-1]);
            # print(colored.magenta(hf.readline()[:128]));
            # print(colored.magenta(hf.readline()[:64]));
            print(colored.magenta('"'+hf.readline()[:32]+'"'));
        else:
            print(colored.red(" File NOT OK : ") + hashfile);
            hash = str(0);
            start = time.time();
            pv_cmd = 'pv -i 0.1 -w 80 -bpetrT "'+filename+'" | md5sum';
            #print(pv_cmd);
            # pv_out = os.system(pv_cmd);
            pv_out = os.popen(pv_cmd).read();
            print(pv_out);
            # print(pv_out);
            # hash = get_digest(filename);
            ende = time.time();
            cmd3 = "echo \"" + pv_out[:-2] + filename + "\" > " + hashfile;
            print(colored.blue(" cmd : ", cmd3));
            os.system(cmd3);
            # print("blake2b : " + hash);
            print( colored.red(' TIME      : '), end='' );
            print( colored.red('{:5.3f}s'.format(ende-start)) );
        
        print(" ------------------------------------------------");
        f.close();
        
    all_size = convert_size(all_size);
    print("\n  All Sizes: "+colored.green(all_size));
        
# -----------------------------------------------------------------------------
# - Python Main
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    sys.exit(main());
